per avere una copia del progetto sul proprio computer:

**git clone https://gitlab.rm.ingv.it/franco.mele/decluster.git**



Una buona documentazione di rassegna dei metodi di declustering si trova in:
http://www.corssa.org/export/sites/corssa/.galleries/articles-pdf/vanStiphout_et_al.pdf_2063069299.pdf 

Articolo originale di Reasenberg: <br>
Reasenberg, P. (1985), Second-order moment of central California seismicity, 1969-82,J. Geophys. Res.,90, 5479–5495.





| programma  |           |  linguaggio|
|----------|:-------------|------:|
| main_program.f90| è il programma da usare, ottenuto dall'originale <br> dell'USGS, con alcune modifiche sul formato di input, <br> con l'aggiunta di alcune opzioni sulla selezione dei terremoti da utilizzare (profondità). <br> Inoltre si selezionano anche le sequenze più importanti in base alla magnitudo equivalente e al numero di eventi.|F90 |
|ingv_earthquakes.py|estrae da webservice ingv i terremoti in formato utile per main_program.f90. <br> Vedere sotto le specifiche del formato.|python 3.*|
| cluster2000x.f|  programma originale distribuito da USGS |  F77|
| cluster2000x.f90 | versione modificata in Fortran 90|   F90 |
| f77\_to\_f90.f90 | trasforma un file sorgente scritto in F77 (.for o .f) <br> in un altro file sorgente in Fortran 90 (.f90) (ma poi occorre lavorarci ancora parecchio per compilarlo senza errori) |    F90 |


# ingv\_earthquakes.py

Procedura in python (3.x) che estrae le localizzazioni e le preferred magnitudes dei terremoti utilizzando i webservices dell'INGV (usa l'opzione format=text). 
L'uscita viene rielaborata in modo da produrre un file in formato utile per il programma main\_program.f90, rielaborazione di cluster2000x.f (distribuito da USGS).

Il programma permette di selezionare gli eventi per coppie max-min di data, latitudine, longitudine, profondità, magnitudo. Permette inoltre di scegliere se mantenere le magnitudo preferred originali (ML, Md, Mw, Mwp, Mwpd, mb) o se trasformare tutte le Mw, Mwp ed Mwpd in ML con una delle due possibili relazioni (Scogmamiglio et al. o Di Bona) e le mb con l'unica relazione disponiile calcolata da Di Bona. 




# ingv\_earthquakes.py: esempio di riga del file di output ( input per main\_program.f90)

 Si tratta del formato di tipo FREE FORMAT della procedura main_program.f90 <br> (5 interi e 4 reali separati da blank o tab): 
 
 <br>
2020   12   02   18   02   42.69330   13.28520   11.1   3.1   ML   3 km E Accumoli (RI)



| anno|mese|giorno|ore|minuti|latit|longit|profondità|magnit|campi ignorati da main_program|
|---|---|---|---|---|---|---|---|---|---|
|2020 | 12|02|18|02|42.6933|13.2852|11.1|3.1|ML   3 km E Accumoli (RI)|



Ricordarsi di riordinare l'uscita in data crescente.

Ricordarsi anche di imporre il punto con cifre decimali nelle ultime 4 colonne che prevedono reali (se si passa per un excel).

# ingv\_earthquakes.py: trasformazione delle magnitudo in ML

La procedura main_program.f90 si aspetta come input delle ML per poi trasformarle in Mw con una delle due relazioni Scognamiglio et al o Di Bona. Poiché il web service in formato testo non fornisce magnitudo omogenee ma solo le preferred, è possibile scegliere come opzione di trasformare le Mw, Mwp ed Mwpd in ML con una delle due relazioni citate.
Se si fa tale scelta, le mb verranno trasformate in ML con la relazione mb-ML di Di Bona.

# main\_program.f90: (de-)clustering con Reasenberg 

Il programma di Reasenberg cluster2000x.f viene fornito da USGS al seguente indirizzo:

https://www.usgs.gov/software/cluster2000

Il programma è in Fortran77. Ne ho fatto una versione modificata in Fortran90 compilabile in macOS (Big Sur), nonché su Windows con Visual Studio 2005 (compilatore INTEL Fortran). Il file da compilare in questo progetto è "main_program.f90", compilato con "make". Ho verificato che è possibile compilarlo su Windows anche senza il compilatore INTEL; occorre in tal caso installare mingw-64 e gfortran.

La procedira di Reasenberg è storicamente una delle prime mai realizzate per il declustering. L'intenzione del "declusterizzatore" è di sostituire un cluster di eventi (ciò che qualcuno chiamerebbe "sequenza") con un unico terremoto equivalente. Il risultato finale costituito dalla somma degli eventi non clusterizzati più gli eventi equivalenti dovrebbe fornire un catalogo "declusterizzato" in cui ci si aspetta che i terremoti siano distribuiti secono la legge di Poisson.

L'algorito di Reasemberg è stato criticato da molti e viene considerato superato, sia per l'arbitrarietà di alcune scelte, sia per il fatto che i cotaloghi che ne risultano di fatto si discostano dalle distribuzioni poissoniane.

Tuttavia, per i nostri scopi che sono di analisi qualitativa del passato, senza alcuna pretesa di valutazione quantitativa, l'algoritmo di Reasenber va benone.

Di fatto il nostro intento è: individuare le sequenze principali già avvenute.

Il programma si aspetta come input magnitudo omogenee. E' possibile scegliere se usare una relazione ML-> Mw oppure no.


# main\_program.f90: output

### cluster.anno  
lista di tutti i terremoti con an notazioni (cluster di appartenenza e simbolo per il plot)

### cluster.clu   
lista dei terremoti appartenenti a clusters, in ordine di tempo origine

### cluster.clu2  
lista dei terremoti appartenenti a clusters selezionati, in ordine di cluster.

### cluster.dec   
Catalogo declusterizzato (il vero obiettivo di Reasenberg), che include i terremoti non clusterizzati e i terremoti equivalenti in sostituzione dei cluster.

### cluster.out   
lista dei cluster con parametri di cluster (n.eqs, mag equivalente, ipocentro equivalente, ecc...)

### clister.out2  
lista (nostra) di cluster selezionati.










#compilazione

Il file Makefile, attivato con il semplice comando "make" su mac o Linux, serve per compilare main\_program.f90


per la compilazione del programma originario di Reasenberg su macOS (big sur):

gfortran  -g  -o cluster2000x cluster2000x.o -L/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/lib


1. 
