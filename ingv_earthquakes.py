# ingv_earthquakes.py
#
# programmed on May 7th 2021 by F. Mele.
# Estraggo tramite webservice dell'INGV il catalogo dei terremoti
# nel formato FREE FORMAT (opzione 5) utile come input del programma cluster2000x di Reasenberg,
# da me modificato nella versione "main_program.f90".
# Il programma di Reasenberg calcola la magnitudo equivalente di ogni cluster
# sommando i momenti sismici dei terremoti del cluster. Essi sono calcolati con una formula
# di Kanamori che lega magnitudo a momento sismico. Una volta calcolato il momento sismico
# totale del cluster, la magnitudo equivalente è calcolata con la formula inversa della precedente.
#
# Il formato FREE è costituito da 5 interi e 4 reali separati da blank:
# yyyy mm dd HH MM lat lon depth mag
#
# Gli altri campi estratti da questa procedura sono ignorati dai programmi cluster2000x.f e main_program.f90
import pandas as pd

def main():
#  http://terremoti.ingv.it/events?last_nd=-1&starttime=2021-05-08&endtime=2021-05-15&minmag=2&maxmag=10&wheretype=pointradius&box_search=Mondo&minlat=-90&maxlat=90&minlon=-180&maxlon=180&municipio=&lat=42&lon=13&maxradiuskm=30&mindepth=-10&maxdepth=1000


  typeq = input(" selezione rettangolare (1) o circolare (2) ? ")

  if( typeq == "1" ):
       minlat = input(" latitudine minima (ES: 35.5) ")
       maxlat = input(" latitudine massima (ES: 48.0) ")
       minlon = input(" longitudine minima (ES: 5.0) ")
       maxlon = input(" longitudine massima (ES: 20.0) ")
  elif( typeq =="2" ):
       lat = input(" latitudine del centro  ")
       lon = input(" longitudine del centro ")
       radius = input(" raggio del cerchio (km) ")
  else:
       print(" non hai scelto né 1 né 2 ")
       exit()

  mindepth = input(" profondita' minima (ES: 0) ")
  maxdepth = input(" profondita' massima (ES: 60.0) ")
  minmag = input(" magnitudo minima  (ES: 0.0) ")
  maxmag = input(" magnitudo massima (ES: 10.0) ")
  startt = input(" data di inizio nella forma yyyy-mm-dd ")
  endt   = input(" data di fine nella forma yyyy-mm-dd ")
  mlyn = input(" trasformo tutte le magnitudo in ML ? (1=sì,  2=no) ")
  mltrue=False       # se mltrue=True, trasformo le tutte le magnitudo in ML
  mldb=True          # se mltrue=True e mldb=True, uso per la trasformazione
                     # la relazione Di Bona, altrimenti uso la Scongn.Tin.Mich.
  if(mlyn =="1"):
      mltrue=True
      print(" mltrue è  ",mltrue)
      print("        per la trasformazione Mw -> ML uso la relazione:")
      print("        Scognamiglio Tinti Michelini (2009)  (1)")
      stmyn=input("                           o Di Bona (2016)  (2) ? ")
      if(stmyn =="1"):
          mldb=False
          print(" mldb è  ",mldb)
  print(" estraggo i dati del periodo ",startt, " ", endt)


  if( typeq =="1" ):
     url = "http://webservices.ingv.it/fdsnws/event/1/query?minlat="+minlat+"&maxlat="+maxlat+ \
          "&minlon="+minlon+"&maxlon="+maxlon+ \
          "&mindepth="+mindepth+"&maxdepth="+maxdepth+ \
          "&starttime="+startt+"&endtime="+endt+ \
          "&minmag="+minmag+"&maxmag="+maxmag+"&format=text"
  else:
     url = "http://webservices.ingv.it/fdsnws/event/1/query?wheretype=pointradius"+ \
           "&lat="+lat+ \
           "&lon="+lon+ \
           "&maxradiuskm="+radius+ \
           "&mindepth="+mindepth+"&maxdepth="+maxdepth+ \
           "&starttime="+startt+"&endtime="+endt+ \
           "&minmag="+minmag+"&maxmag="+maxmag+"&format=text"

  url0 = url.replace(" ","")
  print(url0)

#  data = pd.read_csv(url,sep='|',header=0,names=['evid','ot','lat','lon','dep','author','catalog','contrib','mtype','mag','magauthor','eventlocname'])
  data = pd.read_csv(url0,sep='|',header=0, \
          dtype={'Latitude':float,'Longitude':float,'Depth/Km':float,'Magnitude':float})
  print(data)
# datas = data.replace("/","_")

  df = pd.DataFrame(data, columns= ['Time','Latitude','Longitude','Depth/Km','MagType','Magnitude','EventLocationName'])

  filout = startt+"_"+endt+".txt"
  filo = filout.replace("-","")
  filo = filo.replace(" ","")
  filo = filo.replace(":","")


  with open(filo, 'w') as f:
      ii=0
      for ind in df.index[::-1]:
        ii=ii+1
        mymag = df['Magnitude'][ind]
        myty  = df['MagType'][ind]

        # qui applico varie relazioni per trasformare tutto in ML.
        # Da Di Bona (eq. 15) :  mb = 0.99 ML + 0.02       segue
        #                        ML = ( mb - 0.02 )/ 0.99
        #
        # La relazione calcolata da Di Bona per Mw è (eq. 22):
        #                        Mw = 0.915 ML + 0.31      segue
        #                        ML = (Mw - 0.31) / 0.915
        #
        # La relazione di Scognamiglio, Tinti, Michelini (2009) è (pag. 2234):
        #                        Mw = 0.93 ML + 0.20       segue
        #                        ML = ( Mw - 0.20 )/ 0.93
        #

        if(mltrue):
            if( df['MagType'][ind]=="Mw" or df['MagType'][ind]=="Mwp" or df['MagType'][ind]=="Mwpd" ):
                if(mldb):
                    mymag = (  df['Magnitude'][ind] - 0.2 ) / 0.93      # da Di Bona
                    myty  = "ML(DiBo; Mw "+str(df['Magnitude'][ind])+")"
                else:
                    mymag = (  df['Magnitude'][ind] - 0.31 ) / 0.915    # da Scognamiglio et al.
                    myty  = "ML(S.T.M.; Mw "+str(df['Magnitude'][ind])+")"
            elif( df['MagType'][ind]=="mb"  ):
                 mymag = ( df['Magnitude'][ind] - 0.02 ) / 0.99
                 myty  = "ML(DiBo; mb "+str(df['Magnitude'][ind])+")"

        print(df['Time'][ind][0:4]," ",\
            df['Time'][ind][5:7]," ",\
            df['Time'][ind][8:10]," ",\
            df['Time'][ind][11:13]," ",\
            df['Time'][ind][14:16]," ",\
            "%.5f" % round(df['Latitude'][ind],5)," ",\
            "%.5f" % round(df['Longitude'][ind],5)," ",\
            "%.1f" % round(df['Depth/Km'][ind],1)," ",\
             myty," ", \
            "%.2f" % round(mymag,2)," ", df['EventLocationName'][ind])

        print(df['Time'][ind][0:4]," ",\
             df['Time'][ind][5:7]," ",\
             df['Time'][ind][8:10]," ",\
             df['Time'][ind][11:13]," ",\
             df['Time'][ind][14:16]," ",\
             "%.5f" % round(df['Latitude'][ind],5)," ",\
             "%.5f" % round(df['Longitude'][ind],5)," ",\
             "%.1f" % round(df['Depth/Km'][ind],5)," ",\
             "%.2f" % round(mymag,2)," ", \
             myty," ", \
             df['EventLocationName'][ind],file=f)

  print(" numero eventi: ",ii, " scritti nel file ",filo)


if __name__ == "__main__":
    main()
