!      PROGRAM CLUSTER2000X 
!
!    RECOGNIZE CLUSTERS IN SPACE-TIME IN AN EARTHQUAKE CATALOG
!
!    VERSION CLUSTER2000 IS A MODIFICATION OF CLUSTER5, INCORPORATING
!    YEAR-2000 FORMAT COMPATIBILITY FOR INPUT FILES.
!
!    This code is a pre-release of a Y2K-version, developed for Rob Wesson.
!    It does not implement full Y2K compatibility. For example, the time calculations
!    are NOT Y2K-compatible because the century is ignored, so this code will NOT WORK
!    if the catalog bridges a century change. A real Y2K version is coming, which 
!    will accept catalogs that bridge century change.
!
!    When hypoinverse-2000 format is used:
!      - century is ignored (year in output files is always 19xx)
!      - equivalent events are composed as follows. 
!            - date is date of largest event in cluster (year is always 19xx)
!            - magnitude reflects summed moment of cluster
!            - hypocenter is centroid of cluster
!            - error ellipses and erh,erz are taken from largest event in cluster
!
! 
!
!
!      -  USES HYPOCENTRAL ERROR ESTIMATES IN DISTANCE CALCULATION
!
!C     DESCRIPTION OF VARIABLES:
!
!          list     pointer from event to cluster number
!          nc       number of events in cluster
!          nclust   number of clusters
!          n        index for new cluster number
!          ctim0    time of first event in cluster
!          ctim1    time of largest event in custer
!          ctim2    time of second largest event in cluster
!          clat,clon,cdep   position of 'equivalent event' corresponding
!                   to cluster
!          cmag1    magnitude of largest event in cluster
!          cmag2    magnitude of second largest event in cluster
!          ibigst   event index pointing to biggest event in cluster
!          cdur     duration (first event to last) of cluster
!          cmoment  summed moment of events in cluster
!
!          tau      look-ahead time (minutes) for building cluster.
!          taumin   value for tau when event1 is not clustered
!          rtest    look-around (radial) distance (km) for building cluster
!          r1       circular crack radius for event1
!          rmain    circular crack radius for largest event in current cluster
!
!          xmeff    "effective" lower magnitude cutoff for catalog.
!                   xmeff is raised above its base value 
!                   by the factor xk*cmag1(list(i)) during clusters.
!
!          rfact    number of crack radii (see Kanamori and Anderson,
!                   1975) surrounding each earthquake within which to
!                   consider linking a new event into cluster.
!
!         mpref    array of 4 integers specifying preferences for 
!                selection of magnitude from hypoinverse record.
!                  mpref(1) is the most prefered value, if it exists.
!                  mpref(2) is the next-most prefered,  if it exists.
!                  mpref(3) is the third-most prefered, if it exists.
!                  mpref(4) is the least-prefered.
!
!                   where  1=fmag; 2=amag; 3=BKY-Mag (B); 4=Recalculated ML (L)
!
!               for example, mpref=(3,1,2,4) means
!                    first use BKY (B) mag.
!                  if it doesnt exist, next use fmag 
!                  if it doesnt exist, next use amag 
!                  if it doesnt exist, use Recalculated ML (L).
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!      input/output units:
!          unit 1       scratch file on scr:
!          unit 2       output - cluster summary
!          unit 3       input  - earthquake catalog to read
!          unit 7       output - original catalog, annotated to show 
!                                cluster membership
!          unit 8       output - declustered catalog: original catalog with
!                                clusters removed and replaced by their 
!                                equivalent events.
!          unit 9       output - cluster catalog: all events that belong to 
!                                a cluster (in chronological order, not  
!                                sorted by cluster)
!          
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!     24 giugno 2010. F. Mele
!     modificato il trattamento del formato libero per leggere anche ore e minuti
!     e profondit� (il formato libero di reasenberg usava solo anno,mese,giorno senza
!     uso della profondit�).  In seguito ho aggiunto anche ore e minuti.
!     Poi, aggiungo la selezione dei cluster con magnitudo equi�valente
!     superiore a una magnitudo data e numero minimo di eventi (in
!     genere 2). 
!      I parametri che definiscono la scelta sono:
!           rmag_min = magnitudo equivalente del cluster minima per
!                      selezionarlo (ES: 3.0)
!           nc_min   = numero minimo di eventi del cluster per
!                      selezionarlo (ES: 2)
!      A questa prima scelta di clusters, aggiungo anche i cluster che,
!      pur con magnitudo equivalente inferiore a rmag_min, hanno molti
!      eventi; i parametri che definiscoo la scelta sono:
!           nc_suf   = numero di eventi del cluster sufficiente a farlo
!                      selezionare (ES:20. Indipendentemente dalla mag
!                      equivalente) purch� almeno un evento abbia
!                      magnitudo maggiore o uguale a rmag_suf (ES. 2.5).
!           rmag_suf = magnitudo raggiunta da almeno uno degli eventi
!                      del cluster selezionato per numero di eventi (ES: 2.5)
!       
!      USE IFPORT                             
                                                                        
      program main 
                                                                        
      dimension clat(200000),clon(200000),cdep(200000) 
      dimension cmag1(200000),cmag2(200000),cdur(200000),cmoment(200000) 
      dimension mpref(4), zmag(4) 
      data  nmag, mpref /4, 3,1,2,4/ 
      integer*2 ctim0(5,200000),ctim1(5,200000),ctim2(5,200000) 
      integer*2 list(2200000),nc(200000),opened(200000) 
      integer*2 jcent, icent 
      integer*2 itime(5),jtime(5),ltime(5),ytim1(5),ytim2(5) 
      integer   fore, ibigst(200000), ibigx 
                                                                        
      character*1 q1,q2,ichr,ins,iew,insx,iewx,fflag,aflag,cm1,cm2 
      character*2 rmk2 
      character*5 dum5 
      character*3 eid1, eid2 
      character*1 blank/' '/ 
      integer zero /0/ 
      double precision dif 
      character*80 catalog 
      character*132 stamp 
      logical cluster 
      common/a/ rfact,tau 
      character*80 workdir 
      data list,cmag1/2200000*0,200000*0./ 
      character*8 date8 
      character*10 time10 
! modificato da me per estendere il tempo minimo di associazione.       
                                                                        
      data tau0,taumin,taumax,p1,xk /2880., 2880., 14400., 0.99, .5/    ! da 2 a 10 giorni
!      data tau0,taumin,taumax,p1,xk /7200., 7200., 14400., 0.99, .5/   ! da 5 a 10 giorni
!      data tau0,taumin,taumax,p1,xk /14400., 14400., 14400., 0.99, .5/ 
      data xmeff,rfact,ierradj /1.5, 10.0, 1/ 
      data q1,q2,ins,iew,fflag,aflag /' ',' ',' ',' ',' ',' '/ 
      data isw/0/ 
      nmagrej = 0 
      ntimrej = 0 
      nquarej = 0 
      ifile = 1 
                                                                        
      write(*,*)' ATTENZIONE! QUESTA VERSIONE TRASFORMA LE MAG ML' 
      WRITE(*,*)' IN MAGNITUDO Mw SECONDO LA RELAZIONE DI' 
      WRITE(*,*)' Scognamiglio L., E. Tinti, A. Michelini (2009):' 
      !! Real-Time Determination of Seismic Moment Tensor for the Italian Region.
      !! B.S.S.A., Vol.99,No.4,pp.2223�2242, August 2009, doi:10.1785/01200801040.1785/01
      WRITE(*,*)'     Mw = 0.93  Ml + 0.20' 
      write(*,*)'  ' 
      write(*,*)' o, in alternativa, con la relazione Di Bona (2016)'
      write(*,*)'     Mw = 0.915 Ml + 0.31'
      write(*,*)' '
      write(*,*)' il formato libero (scelta 5 in seguito) prevede:' 
      write(*,*)' year month day hour minute lat lon depth mag' 
      write(*,*)' quindi: 5 interi + 4 reali separati da bianchi'

      write(*,*)' '
      write(*,*)' '
      write(*,*)' IL FILE DI INPUT DEVE AVERE I TERREMOTI IN ORDINE'
      write(*,*)' CRESCENTE DI DATA'
      write(*,*)' '
      write(*,*)' '
                                                                        
!-- OPEN A DIRECT ACCESS SCRATCH FILE FOR CATALOG                       
      open (1, status='scratch',                                        &
     &      access='direct', recl=109)                                  
                                                                        
                                                                        
!-------- sposto qui l'apertura del file per poter memorizzare i parametri usati
            open (22,file='cluster.out2',status='unknown') 
                                                                        
!     Record length for an unformatted file is specified  in bytes      
!     Records are 109 bytes long                                        
                                                                        
                                                                        
!-- SPECIFY THE SOURCE OF THE DATA                                      
   20 write (6,1) 
    1 format (' Enter catalog filename: ') 
      read (5,2) catalog 
    2 format (a) 
      write(22,'(a,a)')' catalog file: ',catalog 
                                                                        
                                                                        
!-- DECLARE THE DATA FORMAT                                             
        write (6, 3) 
    3   format (' Data format for input file '/                         &
     &  '(1=HYPOINV, 2=HYPOINV-2000, 3=HYPO71, 4=HYPO71-2000,'/         &
     &  '  5=FREE, 6=zmap):')                                           
        read (5, *) infmt 
        write(22,'(a,i5)')' FORMAT: ',infmt 
                                                                        
!-- SPECIFY STARTING AND ENDING DATES TO PROCESS                        
      write (6, 52) 
   52 format ('Enter EARLIEST and LATEST YEAR to be accepted:') 
      read (5, *) IY1, IY2 
      write(22,'(a,i5,i5)')' years: ',IY1, IY2 
                                                                        
!-- SPECIFY MINIMUM MAGNITUDE TO ACCEPT                                 
      write (6, 50) 
   50 format (' Enter MINIMUM MAGNITUDE to be accepted (ES: 0.01): ') 
      read (5, *) xmagcut 
      write(22,'(a,f5.2)')' MINIMUM MAGNITUDE to be accepted: ',xmagcut 
                                                                        
                                                                        
!-- F. Mele (2014-11-13): SPECIFY MAX DEPTH:                            
      write(6,*)' Enter MAX DEPTH allowed:' 
      read(5, *)crudep 
      write(22,'(a,f7.1)')' MAX DEPTH allowed:',crudep 
                               
!-- F. Mele (2021-may-09): Apply Di Bona or Scognamiglio et al. 
!           relation to compute Mw from ML.
555   write(6,*)' ML -> Mw; nessuna trasformazione  (M generica) (0)' 
      write(6,*)'               relation Scognamiglio et al.2009 (1)'
      write(6,*)'                                or Di Bona 2016 (2)'
      read(5, *)MlMwswc
      if(MlMwswc.ne.1 .and. MlMwswc.ne.0 .and. MlMwswc.ne.2)goto 555   

!-- SPECIFY KEY PARAMETERS                                              
      write (6, 51) 
   51 format (' Enter RFACT value (default=10): ') 
      read (5, *) rfact 
      write(22,'(a,f6.2)')' RFACT value (default=10): ',rfact 
                                                                        
                                                                        
!-- SPECIFY HANDLING OF EPICENTRAL ERRORS                               
      write (6, 53) 
   53 format (' Indicate method for treating hypocentral',              &
     &  ' location errors: '/' (1=IGNORE ERRORS, 2=ADJUST FOR ERRORS)') 
      read (5, *) ierradj 
      if(ierradj .eq. 1)then 
          write(22,'(a)')' ERROR CORRECTION ?  1=IGNORE ERRORS' 
      else 
          write(22,'(a)')' ERROR CORRECTION ?  2=ADJUST FOR ERRORS' 
      endif 
!--  READ DATA FROM THE NAMED CATALOG                                   
        write (6,29) catalog 
   29   format (' OPENING USER FILE ', a) 
        open (3,file=catalog,status='old',err=20) 
        irec = 1 
        iorec = 1 
   31   goto (101, 102, 103, 104, 105, 106) infmt 
                                                                        
                                                                        
!--HYPOINVERSE FORMAT - INPUT                                           
  101 READ (3, 13, err=900, END=35) itime,lat1,ins,xlat1,lon1,iew,      &
     &               xlon1,dep1,amag1,                                  &
     &               e1az,e1dip,e1, e2az,e2dip,e2,                      &
     &               fmag1,eid1,rmk2,                                   &
     &               erh1,erz1, cm1,altmag1, cm2,altmag2                
                                                                        
      if(itime(1).lt.50)then 
         icent = 20 
         itime(1) = itime(1) + 2000 
      else 
         icent = 19 
         itime(1) = itime(1) + 1900 
      endif 
                                                                        
   13 FORMAT (5I2,4X,i2,a1,F4.2,i3,a1,                                  &
     &       F4.2,F5.2,F2.1,                                            &
     &       T50,f3.0,f2.0,f4.2, f3.0,f2.0,f4.2,                        &
     &       T68,F2.1,a3,T77,a2,                                        &
     &       t81, 2f4.2, t115, a1, f3.2, 3x, a1, f3.2)                  
!---Magnitude selection for Hypoinverse according to Dave Oppenheimer   
!--Choose magnit from preference list. Search down the list of mags in
!  the preferred order until a non-zero magnitude is found.             
        zmag(3)=0. 
        zmag(4)=0. 
!--Find the Berkeley & local mag if present                             
        if (cm1.eq.'B') zmag(3)=altmag1 
        if (cm1.eq.'L') zmag(4)=altmag1 
        if (cm2.eq.'B') zmag(3)=altmag2 
        if (cm2.eq.'L') zmag(4)=altmag2 
!--Assemble preference list                                             
        zmag(1)=fmag1 
        zmag(2)=amag1 
!--The preferred mag is the first non-zero one                          
        do i=1,nmag 
          xmag1=zmag(mpref(i)) 
          if (xmag1.gt.0) goto 108 
        end do 
        icent = 19 
        GOTO 108 
                                                                        
!--HYPOINVERSE-2000 FORMAT - INPUT                                      
  102 READ (3, 2013, err=900, END=35) icent, itime, lat1,ins,xlat1,     &
     &               lon1,iew,xlon1,                                    &
     &               dep1,amag1,                                        &
     &               e1az,e1dip,e1, e2az,e2dip,e2,                      &
     &               fmag1, eid1, rmk2,                                 &
     &               erh1,erz1, cm1,altmag1, cm2,altmag2,               &
     &               xmag1                                              
                                                                        
                                   ! itime(1) diventa l'anno            
      itime(1)=itime(1)+icent*100 
                                                                        
 2013 FORMAT (6i2,4x, i2,a1,F4.2,                                       &
     &        i3,a1,F4.2,                                               &
     &        F5.2,F3.2,                                                &
     &        T53,f3.0,f2.0,f4.2, f3.0,f2.0,f4.2,                       &
     &        T71,F3.2, a3, T81,a2,                                     &
     &        t86, 2f4.2, t123, a1, f3.2, 3x, a1, f3.2,                 &
     &        t148, f3.2)                                               
        GOTO 108 
                                                                        
                                                                        
!--HYPO71 FORMAT - INPUT                                                
  103 read (3, 10, err= 900, END=35) itime,lat1,ins,xlat1,lon1,iew,     &
     &                              xlon1,dep1,xmag1,erh1,erz1,q1       
                                                                        
      if(itime(1).lt.50)then 
         icent = 20 
         itime(1) = itime(1) + 2000 
      else 
         icent = 19 
         itime(1) = itime(1) + 1900 
      endif 
                                                                        
                                                                        
   10 format (3i2,1x,2i2,6x,i3,a1,f5.2,1x,i3,a1,f5.2,                   &
     &        2x,f5.2,3x,f4.2,18x,f4.1,1x,f4.1,1x,a1)                   
      icent = 19 
      goto 108 
                                                                        
!--HYPO71-2000 FORMAT                                                   
  104 read (3, 2010, err= 900, END=35) icent,itime,lat1,ins,xlat1,      &
     &                     lon1,iew,xlon1,dep1,xmag1,erh1,erz1,q1       
                                                                        
                                   ! itime(1) diventa l'anno            
      itime(1)=itime(1)+icent*100 
                                                                        
 2010 format (4i2,1x,2i2,6x,i3,a1,f5.2,i4,a1,f5.2,                      &
     &        f7.2,2x,f5.2,17x,2f5.1,1x,a1)                             
      goto 108 
                                                                        
!--FREE FORMAT - INPUT                                                  
  105 if(isw.eq.0)then 
         isw=1 
         write(*,*)' give err-h err-z in km ' 
         read(*,*) erh1,erz1 
         write(22,*)' err-h err-z in km: ',erh1,erz1 
      endif 
                                                                        
!      read (3, *, err= 900, END=35) icent, itime,                      
!     1     xjlat, xjlon, dep1, xmag1                                   
                                                                        
! nella mia versione del formato libero itime(1) � l'anno.              
! impongo che il formato libero abbia l'anno in prima colonna.          
                                                                        
      read (3, *, err= 900, END=35) itime,                              &
     &     xjlat, xjlon, dep1, xmag1                                    
                                                                        
      write(*,*)irec, itime,                                            &
     &     xjlat, xjlon, dep1, xmag1                                    
      icent = itime(1)/100 
                                                                        
      lat1=xjlat 
      xlat1=(xjlat-lat1)*60.0 
      lon1= xjlon 
      xlon1=(xjlon-lon1)*60.0 
                                                                        
      q1=' ' 
      iew=' ' 
      ins=' ' 
                                                                        
      goto 108 
                                                                        
                                                                        
! infmt=106  "ZMAP" FORMAT:                                             
                                                                        
  106 if(isw.eq.0)then 
         isw=1 
         write(*,*)' give err-h err-z in km ' 
         read(*,*) erh1,erz1 
         write(22,*)' err-h err-z in km: ',erh1,erz1 
      endif 
                                                                        
! qui impongo il formato ZMAP                                           
! Lon       Lat     yr     mo   dy  Mag     dep   hh  mm              
! 15.0833 40.8500   1981   1    6   2.80   5.00   17  13   
!                                                                       
      read (3, *, err= 900, END=35) xjlon, xjlat,                       &
     & itime(1),itime(2),itime(3),                                      &
     & xmag1, dep1,                                                     &
     & itime(4), itime(5)                                               
                                                                        
      icent = itime(1)/100 
                                                                        
      lat1=xjlat 
      xlat1=(xjlat-lat1)*60.0 
      lon1= xjlon 
      xlon1=(xjlon-lon1)*60.0 
                                                                        
      q1=' ' 
      iew=' ' 
      ins=' ' 
                                                                        
      goto 108 
                                                                        
  108 irec = irec + 1 
                                                                        
!- debug write (6,*) irec,zmag(1),zmag(2),zmag(3),zmag(4),zmag(5),xmag1
                                                                        
!--     ... ACCEPT ONLY EVENTS PASSING INPUT CRITERIA...                
                                                                        
!     APPLY MAGNITUDE CUT:                                              
        if (xmag1 .lt. xmagcut) then 
          nmagrej = nmagrej + 1 
          goto 31 
        end if 
!     REMOVE QUARRY SHOTS:                                              
       if (rmk2(1:1) .eq. 'Q' .or. rmk2(1:1) .eq. 'q' .or.              &
     &     rmk2(2:2) .eq. 'Q' .or. rmk2(2:2) .EQ. 'q') then             
         nquarej = nquarej + 1 
         goto 31 
       end if 
!     APPLY TIME LIMITS:                                                
        if (itime(1) .lt. iy1 .or. itime(1) .gt. iy2) then 
          ntimrej = ntimrej + 1 
          goto 31 
        end if 
                                                                        
!     F.Mele (2014-11-13): APPLY DEPTH LIMITS:                          
                                                                        
        if ( dep1 .gt. crudep)then 
          ndeprej = ndeprej + 1 
          goto 31 
        endif 
                                                                        
! F. Mele. 2021                                                         
! Segue modifica per passare da ML a Mw                                 
                                                                        
!cc Scognamiglio L., E. Tinti, A. Michelini (2009). 
! Real-Time Determination of Seismic Moment Tensor for the Italian Region.
!cc B.S.S.A., Vol.99,No.4,pp.2223�2242, August 2009, doi:10.1785/0120080104
!cc Mw = 0.93 Ml + 0.20


!cc Di Bona M. (2016). A Local Magnitude Scale for Crustal Earthquakes in Italy.
!cc B.S.S.A., Vol. 106, No. 1, pp. 242�258, February 2016, doi: 10.1785/0120150155
!cc Mw = 0.915 Ml + 0.31

!c  da Scognamiglio et al (che fanno una regressione Mw-Ml dalla Hutton-Boore).
!c   passando alla ML Di Bona occorrera'  usare la relazione Di Bona per
!    la Mw
                                                                        
     !  if  MlMwswc is 0 do nothing: accept ML or Mw or mb like they
     !  arrive!
        if( MlMwswc .eq.1)then  
          s_Mw = 0.93 * xmag1 + 0.2        ! Scognamiglio et al. 
          xmag1 = s_Mw 
        else if(MlMwswc .eq.2)then
          s_Mw = 0.915 * xmag1 + 0.31      ! Di Bona
          xmag1 = s_Mw
        endif

!--     ...AND WRITE AN UNFORMATTED RECORD TO A SCRATCH DIRECT-ACCESS FILE
                                                                        
!        write (6,*) iorec,itime,dep1,xmag1                             
!        write (6,*)                                                    
                                                                        
        write(1,rec=iorec)icent,itime,lat1,ins,xlat1,lon1,iew,xlon1,    &
     &             dep1, xmag1,erh1,erz1,q1,dlat1, dlon1, ddep1, rms,   &
     &             nst, amag1, aflag, fmag1, fflag, dum5,               &
     &             e1az,e1dip,e1, e2az,e2dip,e2, eid1                   
                                                                        
        iorec = iorec + 1 
        goto 31 
                                                                        
   35   neq = iorec - 1 
        inrec = irec - 1 
        close (unit=3,status='keep') 
                                                                        
      write (6,39) iy1, iy2, xmagcut, inrec, neq, ntimrej,              &
     &             nmagrej, nquarej                                     
   39 format ('   RANGE OF YEARS TO ACCEPT: ', I4, ' TO ', I4, /        &
     &        ' ...USING MINIMUM MAGNITUDE CUTOFF = ', F5.2 /           &
     &        i12, '   EVENTS READ', /                                  &
     &        i12, '   EVENTS ACCEPTED' /                               &
     &        i12, '   EVENTS WERE OUTSIDE TIME WINDOW' /               &
     &        i12, '   EVENTS WERE REJECTED FOR MAGNITUDE' /            &
     &        i12, '   EVENTS WERE REJECTED AS QUARRY BLASTS')          
                                                                        
      if (ierradj .eq. 1) write (6, 37) 
   37 format ('   LOCATION ERRORS IGNORED in distance calculations') 
      if (ierradj .eq. 2) write (6, 38) 
   38 format ('   LOCATION ERRORS SUBTRACTED in distance calculations') 
                                                                        
                                                                        
!-- GET TO WORK                                                         
      i=0 
      n=0 
      nclust=0 
      open (2,file='cluster.out',status='unknown') 
!      open (22,file='cluster.out2',status='unknown')                   
      open (7,file='cluster.anno',status='unknown') 
      open (8,file='cluster.dec',status='unknown') 
      open (9,file='cluster.clu',status='unknown') 
      open (92,file='cluster.clu2',status='unknown') 
                                                                        
!--- process one event at a time                                        
                                                                        
!     get the ith event                                                 
  100 i=i+1 
      read(1,rec=i,err=500)icent,itime,lat1,ins,xlat1,lon1,iew,xlon1,   &
     &             dep1, xmag1,erh1,erz1,q1,dlat1, dlon1, ddep1, rms,   &
     &             nst, amag1, aflag, fmag1, fflag, dum5,               &
     &             e1az,e1dip,e1, e2az,e2dip,e2, eid1                   
                                                                        
!      write (6,*) i,itime,dep1,xmag1                                   
                                                                        
      if (mod(i,1000) .ne. 0) goto 115
      call fdate(stamp)
      call date_and_time(date8,time10) 
                                                                        
      write (6,110) date8,time10,itime, i, nclust 
  110 format (2x, a,' ',a,'  (',i4,2i2,1x,2i2, ')', i815,               &
     &       ' events read',i6,' clusters found')                       
                                                                        
  115 continue 
                                                                        
!---- calculate tau (days), the look-ahead time for event 1.            
!     When event1 belongs to a cluster, tau is a function of            
!     the magnitude of and the time since the largest event in the      
!     cluster.  When event1 is not (yet) clustered, tau = TAU0          
!  set look-ahead time (in minutes) if event1 is not yet clustered      
      if (list(i) .ne. 0) goto 32 
   30 tau = TAU0 
      goto 40 
                                                                        
!  calculate look-ahead time for events belonging to a cluster          
   32 do  it=1,5 
   33   jtime(it) = ctim1(it,list(i)) 
      enddo 
      call tdif(jtime,itime,dif) 
      t=dif 
      if (t .le. 0.) goto 30 
                                                                        
      deltam = (1.-xk)*cmag1(list(i)) - xmeff 
      denom = 10.**((deltam-1.)*2./3.) 
      top = -alog(1.-p1)*t 
      tau = top/denom 
                                                                        
                                                                        
!      write(*,*)'  ------------ mag e tau ',cmag1(list(i)),tau
!  truncate tau to not exceed taumax, OR DROP BELOW TAUMIN              
      if (tau .gt. taumax) tau = taumax 
      IF (TAU .LT. TAUMIN) TAU = TAUMIN 
                                                                        
   40 continue 
                                                                        
!     keep getting jth events until dif > tau                           
      j=i 
  200 j=j+1 
                                                                        
!--   skip the jth event if it is already identified as being part of   
!     the cluster associated with the ith event                         
      if (list(j) .eq. list(i) .and. list(j) .ne. 0) goto 200 
                                                                        
      read(1,rec=j,err=400)jcent,jtime,lat2,ins,xlat2,lon2,iew,xlon2,   &
     &             dep2, xmag2,erh2,erz2,q2,dlat2, dlon2, ddep2, rms,   &
     &             nst, amag2, aflag, fmag2, fflag, dum5,               &
     &             e1az2,e1dip2,e12, e2az2,e2dip2,e22, eid2             
                                                                        
!      write (6,*) j,jtime,dep2,xmag2                                   
                                                                        
!--- test for temporal clustering                                       
  208 call tdif (itime, jtime, dif) 
                                                                        
!      write (6,*)' debug ', i,j,itime,jtime,lat1,xlat1,lon1,xlon1,     
!     1           lat2,xlat2,lon2,xlon2,list(i), dif, tau               
                                                                        
      if (dif .gt. tau ) goto 400 
                                                                        
!--- test for spatial clustering                                        
!
!
!    NON TROVO LR DUE RIGHE SEGUENTI NELLA VERSIONE ORIGINALE. Verificare !!!
!
      ccmag = MAX(xmag1,xmag2) 
      if ( list(i) .gt. 0)ccmag = cmag1(list(i)) 
                                                                        
      call ctest(itime,lat1,xlat1,lon1,xlon1,dep1,xmag1,erh1,erz1,q1,   &
     &           jtime,lat2,xlat2,lon2,xlon2,dep2,xmag2,erh2,erz2,q2,   &
     &           ccmag,cluster,ierradj)                                 
                                                                        
                                                                        
      if (cluster .eqv. .false.) goto 200 
                                                                        
!---- cluster declared                                                  
!     if event i and event j are both already associated with           
!     clusters, combine the clusters.                                   
      if (list(i) .ne. 0 .and. list(j) .ne. 0) goto 375 
                                                                        
!     if event i is already associat with a cluster, add event j to it
      if (list(i) .ne. 0) goto 300 
                                                                        
!     if event j is already associat with a cluster, add event i to it
      if (list(j) .ne. 0) goto 280 
                                                                        
!--- initialize new cluster                                             
                                                                        
                                                                        
                                                                        
                                                                        
! Infine si applica:                                                    
!  Mw = 2/3 Log Mo - 10.7    (Kanamori 1977).                           
! Ovvero                                                                
!  Log Mo = (Mw + 10.7)  * 3/2                                          
!                                                                       
!  Mo = 10^[ (Mw + 10.7) * 3/2]                                         
!                                                                       
!  Mo1 = 10^[ (Mw1 + 10.7) * 3/2] = [10^(Mw1 *3/2)] * 10^16.05          
!  Mo2 = 10^[ (Mw2 + 10.7) * 3/2] = [10^(Mw1 *3/2)] * 10^16.05          
!  Mo1 + Mo2 = {[10^(Mw1 *3/2)] + [10^(Mw2 *3/2)]} * 10^16.05           
!                                                                       
!  si calcola quindi la quantit� {[10^(Mw1 *3/2)] + [10^(Mw2 *3/2)]} =SUM
!  Si ottiene poi la Mw corrispondente come:                            
!                                                                       
!  Mo12 = SUM * 10^16.05                                                
!  Mw12 = 2/3 Log Mo12 -10.7 = 2/3 [ Log (SUM) + 16.05] -10.7 = 2/3 Log SUM
                                                                        
                                                                        
                                                                        
! NOTA 2010-07-14 (dall'articolo: log Mo = 17 + 1.2 M)                  
!  quindi,  il contributo Mo da due eq. e':                             
!     Mo1 + Mo2 = 10 **(17 +  1.2 * M1)  +  10**(17 +  1.2 * M2)  =     
!               = 10**17 * [ 10**(1.2 M1) + 10**(1.2 M2) ]              
!  quindi Reasenberg evita il fattore moltiplicativo comune 10**17 e calcola semplicemente
!  la somma di destra, applicando poi la formula inversa                
!       Mo / (10**17) = 10** (1.2 M)                                    
!  M = ALOG10[Mo / (10**17)] / 1.2                                      
                                                                        
      n=n+1 
      nclust=nclust+1 
      list(i)=n 
      clat(n)=lat1+xlat1/60. 
      clon(n)=lon1+xlon1/60. 
      cdep(n)=dep1 
      nc(n)=1 
      ibigst(n) = i 
      cmag1(n)=xmag1 
      cmag2(n)=-2. 
! versione Reasenberg:                                                  
!      cmoment(n)=10**(1.2*xmag1)                                       
                                                                        
! Kanamori:                                                             
      cmoment(n)=10**(1.5*xmag1) 
                                                                        
      do  it=1,5 
        ctim1(it,n)=itime(it) 
        ctim2(it,n)=0 
  250   ctim0(it,n)=itime(it) 
      enddo 
      goto 300 
                                                                        
!--- prepare to add ith event to existing cluster                       
  280 l=i 
      k=list(j) 
      lat=lat1 
      xlat=xlat1 
      lon=lon1 
      xlon=xlon1 
      xmag=xmag1 
      ibigx = i 
      dep=dep1 
      do  it=1,5 
  285    ltime(it)=itime(it) 
      enddo 
      goto 320 
                                                                        
!--- prepare to add jth event to existing cluster                       
  300 l=j 
      k=list(i) 
      lat=lat2 
      xlat=xlat2 
      lon=lon2 
      xlon=xlon2 
      dep=dep2 
      xmag=xmag2 
      ibigx = j 
      do  it=1,5 
  305    ltime(it)=jtime(it) 
      enddo 
                                                                        
!---- add new event to cluster                                          
  320 nc(k)=nc(k)+1 
      w1=(nc(k)-1.)/nc(k) 
      w2=  1.0/nc(k) 
      list(l)=k 
                                                                        
!     update cluster focal parameters                                   
      clat(k)=clat(k)*w1 + (lat+xlat/60.)*w2 
      clon(k)=clon(k)*w1 + (lon+xlon/60.)*w2 
      cdep(k)=cdep(k)*w1 + dep*w2 
                                                                        
!     update other cluster parameters                                   
! versione Reasenberg:                                                  
!      cmoment(k) = cmoment(k) + 10**(1.2*xmag)                         
                                                                        
! Versione Kanamori:                                                    
      cmoment(k) = cmoment(k) + 10**(1.5*xmag) 
                                                                        
                                                                        
      if (xmag .gt. cmag1(k)) goto 350 
      if (xmag .le. cmag2(k)) goto 200 
                                                                        
!     current event is second largest event in cluster k                
      cmag2(k)=xmag 
      do  it=1,5 
  330   ctim2(it,k)=ltime(it) 
      enddo 
      goto 200 
                                                                        
!     current event is largest in cluster k                             
  350 cmag2(k)=cmag1(k) 
      cmag1(k)=xmag 
      ibigst(k) = ibigx 
      do  it=1,5 
        ctim2(it,k)=ctim1(it,k) 
  355   ctim1(it,k)=ltime(it) 
      enddo 
      goto 200 
                                                                        
!---- combine existing clusters by merging into earlier cluster         
!     and keeping earlier cluster's identity                            
  375 k=list(i) 
      l=list(j) 
                                                                        
      if (k .lt. l) goto 376 
      k=list(j) 
      l=list(i) 
!     merge cluster l into cluster k                                    
  376 w1=float(nc(k))/float((nc(k)+nc(l))) 
      w2=float(nc(l))/float((nc(k)+nc(l))) 
      clat(k)=clat(k)*w1 + clat(l)*w2 
      clon(k)=clon(k)*w1 + clon(l)*w2 
      cdep(k)=cdep(k)*w1 + cdep(l)*w2 
      cmoment(k)=cmoment(k) + cmoment(l) 
      do  ii=1,neq 
  380   if (list(ii) .eq. l) list(ii)=k 
      enddo 
      nc(k)=nc(k)+nc(l) 
      nc(l)=0 
      nclust=nclust-1 
                                                                        
!     identify largest and second largest magnitude events in           
!     merged cluster                                                    
      if (cmag1(k) .ge. cmag1(l)) then 
            ymag1=cmag1(k) 
            ibigx = ibigst(k) 
            do  it=1,5 
  382         ytim1(it)=ctim1(it,k) 
            enddo 
            if (cmag1(l) .gt. cmag2(k)) then 
                  ymag2=cmag1(l) 
                  do  it=1,5 
  383               ytim2(it)=ctim1(it,l) 
                  enddo 
            else 
                  ymag2=cmag2(k) 
                  do  it=1,5 
  384               ytim2(it)=ctim2(it,k) 
                  enddo 
            end if 
      else 
            ymag1=cmag1(l) 
            ibigx = ibigst(l) 
            do  it=1,5 
  392         ytim1(it)=ctim1(it,l) 
            enddo 
            if (cmag1(k) .ge. cmag2(l)) then 
                  ymag2=cmag1(k) 
                  do  it=1,5 
  393               ytim2(it)=ctim1(it,k) 
                  enddo 
            else 
                  ymag2=cmag2(l) 
                  do  it=1,5 
  394               ytim2(it)=ctim2(it,l) 
                  enddo 
            end if 
      end if 
                                                                        
      cmag1(k)=ymag1 
      cmag2(k)=ymag2 
      ibigst(k) = ibigx 
      do  it=1,5 
        ctim1(it,k)=ytim1(it) 
  395   ctim2(it,k)=ytim2(it) 
      enddo 

!     update duration of merged event                                   
      do  it=1,5 
  396   jtime(it)=ctim0(it,k) 
      enddo 
      call tdif(jtime,itime,dif) 
      cdur(k)=dif/1440. 
                                                                        
      goto 200 
!                                                                       
!---- finish processing ith event                                       
  400 if (list(i) .eq. 0) goto 100 
                                                                        
!     update duration of cluster k for event i                          
      do  it=1,5 
  360   jtime(it)=ctim0(it,list(i)) 
      enddo 
      call tdif(jtime,itime,dif) 
      cdur(list(i))=dif/1440. 
      goto 100 
                                                                        
!---- entire catalog has been searched                                  
!     output results                                                    
                                                                        
  500 neqcl = 0 
      do  i=1,neq-1 
  502   if (list(i) .ne. 0) neqcl=neqcl+1 
      enddo 
      call fdate (stamp) 
       call date_and_time(date8,time10) 
                                                                 
!============================================                           
                                                                        
!c   ABBIAMO PRIMA USATO:                                               
!c  da Scognamiglio et al (che fanno una regressione Mw-Ml dalla Hutton-Boore
!c   passando alla ML Di Bona occorrera'  usare la relazione di Di Bona 
!c        !cc Mw = 0.915 Ml + 0.31
                                  
                                                                        
                                                                        
! Devo applicare ora le relazioni inverse delle precedenti              
! per ritornare alle ML ISIDe                                           
                                                                        
      do  i=1,neq 
                                                                        
!-- MAIN MAG-RESET LOOP                                                 
        read (1, rec=i) icent,itime,lat1,ins,xlat1,lon1,iew,xlon1,      &
     &                dep1,xmag1,erh1,erz1,q1,dlat1, dlon1,             &
     &   ddep1, rms, nst, amag1, aflag, fmag1, fflag, dum5,             &
     &   e1az,e1dip,e1, e2az,e2dip,e2, eid1                             
                                                                        
! formula inversa di quallea gi� applicata di Scognamiglio et al.
! (cos� torniamo alle ML) *ricordarsi di sostituire se si sceglie la Di Bona).
                                                                    

! if MlMwswc is 0 do nothing (magnitudes accepted as they arrive)!
        if( MlMwswc .eq.1)then
           S_ML = (xmag1 - 0.2) / 0.93 ! da Scognam. et al torno a ML
           xmag1 = S_ML 
        else if( MlMwswc .eq.2 )then
           S_ML = (xmag1 - 0.31) / 0.915 ! da Di Bona.torno a ML
           xmag1 = S_ML
        endif
! ATTENZIONE  all'indice  write(1,rec=iorec)icent,itime,lat1,ins,xlat1,lon1,iew,xlon1,    &
        write(1,rec=i)icent,itime,lat1,ins,xlat1,lon1,iew,xlon1,    &
     &             dep1, xmag1,erh1,erz1,q1,dlat1, dlon1, ddep1, rms,   &
     &             nst, amag1, aflag, fmag1, fflag, dum5,               &
     &             e1az,e1dip,e1, e2az,e2dip,e2, eid1                   
 6800 continue 
      enddo 
!============================================                           
                                                                        
      do  i=1,neq 
                                                                        
!-- MAIN OUTPUT LOOP                                                    
      read (1, rec=i) icent,itime,lat1,ins,xlat1,lon1,iew,xlon1,        &
     &                dep1,xmag1,erh1,erz1,q1,dlat1, dlon1,             &
     &   ddep1, rms, nst, amag1, aflag, fmag1, fflag, dum5,             &
     &   e1az,e1dip,e1, e2az,e2dip,e2, eid1                             
                                                                        
      icr = 43 
      if (list(i).ne.0) icr = mod(list(i)-1,26)+65 
      ichr = char(icr) 
                                                                        
!-------- OUTPUT FORMATS--------------------------------------          
!--------- HYPOINVERSE                                                  
  611 format (5i2.2, t15,i2,a1,i4,i3,a1,i4,i5,                          &
     &        t50,i3,i2,i4,i3,i2,i4,i2,a3,                              &
     &        t81,2i4, t129,i10,a1)                                     
!--------- HYPOINVERSE-2000                                             
  612 format (6i2.2, t17,i2,a1,i4,                                      &
     &        i3,a1,i4, i5,                                             &
     &        t53, i3,i2,i4, i3,i2,i4,  i3,a3,                          &
     &        t86, 2i4, t137,i10,a1, i3)                                
!--------- HYPO71                                                       
  613 format (3i2.2,1x,2i2.2,6x,i3,a1,f5.2,1x,i3,a1,f5.2,               &
     &        2x,f5.2,3x,f4.2,18x,f4.1,1x,f4.1,1x,a1,                   &
     &        t82,i10,1x,a1)                                            
!--------- HYPO71-2000                                                  
  614 format (4i2.2,1x,2i2.2,6x,i3,a1,f5.2,1x,i3,a1,f5.2,               &
     &        2x,f5.2,3x,f4.2,18x,f4.1,1x,f4.1,1x,a1,                   &
     &        t84,i10,1x,a1)                                            
!--------------------------------------------------------------         
                                                                        
                                                                        
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC                       
!   DECLUSTERED CATALOG - ONLY UNCLUSTERED EVENTS                       
      if (list(i) .ne. 0) goto 630 
                                                                        
  600 goto (601, 602, 603, 604, 605, 606) infmt 
                                                                        
! HYPOINVERSE format for output                                         
  601 write (8, 611) mod(itime,100),lat1,ins,int(100*xlat1+0.5),        &
     &     lon1, iew, int(100*xlon1+.5), int(100*dep1+.5),              &
     &     int(e1az), int(e1dip), int(100*e1+0.5),                      &
     &     int(e2az),int(e2dip),int(100*e2+0.5), int(10*xmag1+.5),      &
     &     eid1,int(100*erh1+0.5), int(100*erz1+0.5)                    
      goto 650 
                                                                        
! HYPOINVERSE-2000 format for output                                    
  602 WRITE (8, 612) icent,itime,lat1,ins,int(100*xlat1+0.5),           &
     &     lon1, iew, int(100*xlon1+.5), int(100*dep1+.5),              &
     &     int(e1az), int(e1dip), int(100*e1+0.5),                      &
     &     int(e2az),int(e2dip),int(100*e2+0.5),                        &
     &     int(100*fmag1+0.5), eid1,                                    &
     &     int(100*erh1+0.5), int(100*erz1+0.5),                        &
     &     zero, blank, int(100*xmag1+.5)                               
      goto 650 
                                                                        
! HYPO71 format for output                                              
  603 WRITE (8, 613) itime,lat1,ins,xlat1,                              &
     & lon1,iew,xlon1,dep1,xmag1,erh1,erz1,q1                           
      goto 650 
                                                                        
! HYPO71-2000 format for output                                         
  604 write (8, 614) icent,itime,lat1,ins,xlat1,                        &
     & lon1,iew,xlon1,dep1,xmag1,erh1,erz1,q1                           
      goto 650 
                                                                        
! Special free format                                                   
  605  write (8, '(i4,1x,4(i2.2,1x),3(f8.3,1x),f4.2)')                  &
     & (itime(k), k=1,5),                                               &
     &  lat1+xlat1/60, lon1+xlon1/60, dep1, xmag1                       
       goto 650 
                                                                        
! ZMAP format                                                           
! formato ZMAP                                                          
! Lon       Lat     yr     mo   dy    Mag     dep     hh  mm            
! 15.0833 40.8500   1981   1    6    2.80     5.00    17  59
  606  write (8,1096)lon1+xlon1/60,lat1+xlat1/60,                       &
     &  itime(1),itime(2),itime(3),                                     &
     &  xmag1,dep1, itime(4), itime(5)                                  
                                                                        
        goto 650 
                                                                        
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC                            
!   WRITE TO CLUSTERED EVENTS LIST                                      
  630 goto (631, 632, 633, 634, 635, 636) infmt 
                                                                        
! HYPOINVERSE format for output                                         
  631 WRITE (9, 611) mod(itime,100),lat1,ins,int(100*xlat1+0.5),        &
     &     lon1, iew, int(100*xlon1+.5), int(100*dep1+.5),              &
     &     int(e1az), int(e1dip), int(100*e1+0.5),                      &
     &     int(e2az),int(e2dip),int(100*e2+0.5),int(10*xmag1+.5),       &
     &     eid1, int(100*erh1+0.5), int(100*erz1+0.5), LIST(I)          
      goto 650 
                                                                        
! HYPOINVERSE-2000 format for output                                    
  632 WRITE (9, 612) icent,itime,lat1,ins,int(100*xlat1+0.5),           &
     &     lon1, iew, int(100*xlon1+.5), int(100*dep1+.5),              &
     &     int(e1az), int(e1dip), int(100*e1+0.5),                      &
     &     int(e2az),int(e2dip),int(100*e2+0.5),                        &
     &     int(100*fmag1+.5), eid1,                                     &
     &     int(100*erh1+0.5), int(100*erz1+0.5), list(i),               &
     &     blank, int(100*xmag1+.5)                                     
      goto 650 
                                                                        
! HYPO71 format for output                                              
  633 iyy=mod(itime(1),100) 
      WRITE (9, 613) iyy,(itime(jj),jj=2,5),                            &
     & lat1,ins,xlat1,                                                  &
     & lon1,iew,xlon1,dep1,xmag1,erh1,erz1,q1, list(i)                  
      goto 650 
                                                                        
! HYPO71-2000 format for output                                         
  634 write (9, 614) icent,itime,lat1,ins,xlat1,                        &
     & lon1,iew,xlon1,dep1,xmag1,erh1,erz1,q1,list(i)                   
      goto 650 
                                                                        
! free format  (F.Mele)                                                 
  635 write (9,'(i4,1x,4(i2.2,1x),3(f8.3,1x),f4.2,t72,2f5.1,t82,a,i5)') &
     & (itime(k), k=1,5),                                               &
     &  float(lat1)+xlat1/60., float(lon1)+xlon1/60., dep1, xmag1,      &
     &  erh1,erz1,q1, list(i)                                           
       goto 650 
                                                                        
                                                                        
! ZMAP format  (F.Mele)                                                 
  636 write (9,1096)                                                    &
     & float(lon1)+xlon1/60.,float(lat1)+xlat1/60.,                     &
     & itime(1),itime(2),itime(3), xmag1,                               &
     & dep1, itime(4),itime(5)                                          
      goto 650 
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC                       
!   ANNOTATED CATALOG - ALL EVENTS                                      
  650 goto (651, 652, 653, 654, 653, 656) infmt 
                                                                        
! HYPOINVERSE format for output                                         
  651 WRITE (7, 611) mod(itime,100),lat1,ins,int(100*xlat1+0.5),        &
     &     lon1, iew, int(100*xlon1+.5), int(100*dep1+.5),              &
     &     int(e1az), int(e1dip), int(100*e1+0.5),                      &
     &     int(e2az),int(e2dip),int(100*e2+0.5),int(10*xmag1+.5),       &
     &     eid1, int(100*erh1+0.5), int(100*erz1+0.5),                  &
     &     LIST(I), ICHR                                                
      goto 680 
                                                                        
! HYPOINVERSE-2000 format for output                                    
  652 WRITE (7, 612) icent,itime,lat1,ins,int(100*xlat1+0.5),           &
     &     lon1, iew, int(100*xlon1+.5), int(100*dep1+.5),              &
     &     int(e1az), int(e1dip), int(100*e1+0.5),                      &
     &     int(e2az),int(e2dip),int(100*e2+0.5),                        &
     &     int(100*fmag1+.5), eid1,                                     &
     &     int(100*erh1+0.5), int(100*erz1+0.5),                        &
     &     LIST(I), ICHR, int(100*xmag1+.5)                             
      goto 680 
                                                                        
! HYPO71 format for output                                              
  653 WRITE (7, 613) mod(itime(1),100),(itime(jj),jj=2,5),              &
     & lat1,ins,xlat1,                                                  &
     & lon1,iew,xlon1,dep1,xmag1,erh1,erz1,q1, LIST(I),ICHR             
      goto 680 
                                                                        
! HYPO71-2000 format for output                                         
  654 write (7, 614) icent,itime,lat1,ins,xlat1,                        &
     & lon1,iew,xlon1,dep1,xmag1,erh1,erz1,q1, LIST(I),ICHR             
      goto 680 
                                                                        
                                                                        
                                                                        
! ZMAP  format for output                                               
  656 WRITE (7, 613) mod(itime(1),100),(itime(jj),jj=2,5),              &
     & lat1,ins,xlat1,                                                  &
     & lon1,iew,xlon1,dep1,xmag1,erh1,erz1,q1, LIST(I),ICHR             
      goto 680                                          
                                                         
  680 continue 
      enddo 
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC    
                                                                        
!-- write the output file summarizing the run and the listing the clusters
                                                                        
                                                                        
      write (2,503) catalog, inrec, neq, ntimrej, nquarej,              &
     &     nmagrej, xmagcut, neq, nclust, neqcl, date8, time10,         &
     &     rfact, tau0, taumin, taumax, p1, xk, ierradj                 
                                                                        
                                                                        
      write (22,503) catalog, inrec, neq, ntimrej, nquarej,             &
     &     nmagrej, xmagcut, neq, nclust, neqcl, date8, time10,         &
     &     rfact, tau0, taumin, taumax, p1, xk, ierradj                 
                                                                        
                                                                        
  503 format (//8x,'Program: CLUSTER2000X (test version)', //           &
     &    '        SUMMARY OF CLUSTERS IN  ',A,//                       &
     &    i8, ' EVENTS READ', i8, ' EVENTS ACCEPTED',                   &
     &    i8, ' EVENTS WERE OUTSIDE TIME RANGE SPECIFIED',/             &
     &    i8, ' EVENTS WERE REJECTED AS QUARRY BLASTS'/                 &
     &    i8, ' EVENTS REJECTED FOR MAGNITUDE  (MINIMUM MAGNITUDE = ',  &
     &    f5.2, ')' //                                                  &
     &    i10,' earthquakes tested;  ',i6,' clusters identified',5x,    &
     &    i6,' clustered events',5x,a,' ',a,/                           &
     &    '   rfact=',f6.3,'   tau0=',f8.3,'   taumin=',f10.2,          &
     &    '   taumax=',f10.2,'   p1=',f6.3,'   xk=',f5.2,               &
     &    '   ierradj=',i2,///                                          &
     &    '    N  -1ST EVENT-   DUR(DAYS)   NC  -------EQUIVALENT',     &
     &    ' EVENT-------   --LARGEST EVENT--   2ND LARGEST EVENT',      &
     &    ' PCT(F)   DT     DM  '/)                                     
                                                                        
                                                                        
                                                                        
      WRITE(*,*)' Per i cluster che superano una certa magnitudo' 
      write(*,*)' ( e che hanno almeno un numero minimo di eventi):' 
      write(*,*)' fornire Min equivalent mag del cluster   (es: 3)' 
      write(*,*)'       e Min number of events del cluster (es: 2)' 
      read(*,*)rmag_min,nc_min 
      write(*,*)' ' 
      write(*,*)' Per i cluster che superano un certo numero di eventi' 
      write(*,*)' e che hanno almeno un terremoto di magnitudo minima:' 
      write(*,*)' fornire Min number of events del cluster (es: 20) ' 
      write(*,*)'       e il minimo di magnitudo massima necessario a' 
      write(*,*)'       selezionare il cluster (es: 2.5):' 
      read(*,*) nc_suf,rmag_suf 
                                                                        
                                                                        
      write(22,'(a,f3.1,/,a,i3,/,a,/,a,i3,/,a,f3.1)')                   &
     &' Min equivalent mag of the cluster: ',rmag_min,                  &
     &' Min num of evts in the cluster of min equivalent mag: ',nc_min, &
     &'   ',                                                            &
     &' Min number of evts in the cluster of any equiv. mag: ',nc_suf,  &
     &' Min mag of max-mag to select the cluster :',rmag_suf            
                                                                        
                                                                        
                                                                        
!  Loop through clusters, k=1,n                                         
      ngood = 0 
      do  k=1,n 
                                                                        
      ! Versione Reasenberg:                                            
      ! xmag=(alog10(cmoment(k)))/1.2                                   
                                                                        
      ! Versione Kanamori:                                              
      xmag=(alog10(cmoment(k)))/1.5 
                                                                        
      if (nc(k) .eq. 0) goto 700 
                                                                        
      lat=clat(k) 
      xlat=(clat(k)-lat)*60. 
      lon=clon(k) 
      xlon=(clon(k)-lon)*60. 
                                                                        
!---- calculate percentage of cluster duration taken by foreshocks      
      do  it=1,5 
        itime(it)=ctim0(it,k) 
  504   jtime(it)=ctim1(it,k) 
      enddo 
      call tdif(itime,jtime,dif) 
      if (cdur(k) .lt. 0.001) cdur(k)=.001 
      fore =  (dif/14.40) / cdur(k) + .5 
                                                                        
!---- calculate time (days) from largest event to 2nd-largest event.    
!     (positive = aftershock-like,  negative = foreshock-like).         
      do  it=1,5 
        itime(it)=ctim1(it,k) 
  505   jtime(it)=ctim2(it,k) 
      enddo 
      call tdif(itime,jtime,dif) 
      t12dif=dif/1440. 
      xmdif=cmag1(k)-cmag2(k) 
                                                                        
!---- calculate DT, the absolute time difference between largest and    
!                   2nd-largest events,                                 
!           and DM, the magnitude of the second of these events minus   
!                   the magnitude of the first.                         
      if (t12dif .lt. 0.0) then 
         dm =   xmdif 
         dt = - t12dif 
      else 
         dm = - xmdif 
         dt =   t12dif 
      end if 
                                                                        
! Write out cluster summary to "output" file                            
      ichr = char(mod(k-1,26)+65) 
                                                                        
  510 iyy0=mod(ctim0(1,k),100) 
      iyy1=mod(ctim1(1,k),100) 
      iyy2=mod(ctim2(1,k),100) 
      write (2,511) ichr,k,iyy0,                                        &
     &       (ctim0(it,k),it=2,5),cdur(k),nc(k),lat,xlat,               &
     &       lon,xlon,cdep(k),xmag,iyy1,                                &
     &       (ctim1(it,k),it=2,5),cmag1(k),                             &
     &       iyy2,                                                      &
     &       (ctim2(it,k),it=2,5),cmag2(k),fore,DT,DM                   
                                                                        
      if( (xmag .ge. rmag_min .and. nc(k) .ge. nc_min ) .or.            &
     &      (cmag1(k) .ge. rmag_suf .and. nc(k) .ge. nc_suf) )then      
                                                                        
        write(*,*)'mag equiv ',xmag,' maggiore di ',rmag_min 
        write(*,*)'num.ev ',nc(k),' maggiore di ',nc_min 
        write(*,*)'    oppure' 
        write(*,*)'maxmag ',cmag1(k),'maggiore di ',rmag_suf 
        write(*,*)'num.ev ',nc(k),' maggiore di ',nc_suf 
                                                                        
                                                                        
                                                                        
        write (22,511) ichr,k,iyy0,                                     &
     &           (ctim0(it,k),it=2,5),cdur(k),nc(k),lat,xlat,           &
     &       lon,xlon,cdep(k),xmag,iyy1,                                &
     &                    (ctim1(it,k),it=2,5),cmag1(k),                &
     &       iyy2,                                                      &
     &       (ctim2(it,k),it=2,5),cmag2(k),fore,DT,DM,                  &
     &       lat+xlat/60.,lon+xlon/60.,cdep(k),xmag 
      endif 
                                                                        
  511 format (1x,a1,i4,1x,3i2.2,1x,2i2.2,2x,f8.3,2x,i5,                 &
     &        2(i4,f6.2),2f6.2,2(3x,5i2.2,1x,f6.2),i5,1x,f7.3,f6.2,     &
     &        2(2x,f8.3),2x,f6.1,2x,f5.2 )                              
                                                                        
!--- Write out (append) the "equivalent events" to declustered catalog  
!    Use error parameters from largest event in cluster                 
      read(1,rec=ibigst(k),err=903) icent,itime,lat1,insx,xlat1,        &
     &        lon1,iewx,xlon1,                                          &
     &        dep1, xmag1,erh1,erz1,q1,dlat1, dlon1, ddep1, rms,        &
     &        nst, amag1, aflag, fmag1, fflag, dum5,                    &
     &        e1az,e1dip,e1, e2az,e2dip,e2, eid1                        
                                                                        
      goto (691, 692, 693, 694, 695, 696) infmt 
                                                                        
! HYPOINVERSE format for output                                         
  691 WRITE (8, 611) mod(ctim1(1,k),100), (ctim1(it,k),it=2,5),         &
     &                  lat,ins,int(100*xlat+0.5),                      &
     &                  lon, iew, int(100*xlon+.5),                     &
     &                  int(100*cdep(k)+.5),                            &
     &                  int(e1az),int(e1dip),int(100*e1+.5),            &
     &                  int(e2az),int(e2dip),int(100*e2+.5),            &
     &                  int(10*xmag+0.5), eid1,                         &
     &                  int(100*erh1+0.5), int(100*erz1+0.5), k         
      goto 700 
                                                                        
! HYPOINVERSE-2000 format for output                                    
  692 WRITE (8, 612)  icent, mod(ctim1(1,k),100), (ctim1(it,k),it=2,5), &
     &                  lat,ins,int(100*xlat+0.5),                      &
     &                  lon, iew, int(100*xlon+.5),                     &
     &                  int(100*cdep(k)+.5),                            &
     &                  int(e1az),int(e1dip),int(100*e1+.5),            &
     &                  int(e2az),int(e2dip),int(100*e2+.5),            &
     &                  int(100*xmag+0.5), eid1,                        &
     &                  int(100*erh1+0.5), int(100*erz1+0.5), k         
      goto 700 
                                                                        
! HYPO71 format for output                                              
  693 iyy1 = mod(ctim1(1,k),100) 
      WRITE (8, 613) iyy1,                                              &
     & (ctim1(it,k),it=2,5),lat,ins,xlat,                               &
     & lon,iew,xlon,cdep(k),xmag,erh1,erz1,q1, k                        
      goto 700 
                                                                        
! HYPO71-2000 format for output                                         
  694 write (8, 614) icent,mod(ctim1(1,k),100),(ctim1(it,k),it=2,5),    &
     & lat,ins,xlat,                                                    &
     & lon,iew,xlon,cdep(k),xmag,erh1,erz1,q1, k                        
      goto 700 
                                                                        
! Special free format                                                   
  695 write (8, '(i4,1x,4(i2.2,1x),3(f8.3,1x),f3.1)')                   &
     &  (ctim1(it,k),it=1,5), lat+xlat/60.,                             &
     &  lon+xlon/60., cdep(k), xmag                                     
      goto 700 
                                                                        
! ZMAP format                                                           
  696 write (8 ,1096)                                                   &
     &  lon+xlon/60.,lat+xlat/60.,                                      &
     &  ctim1(1,k),ctim1(2,k),ctim1(3,k),                               &
     &  xmag,cdep(k), ctim1(4,k),ctim1(5,k)                             
      goto 700 
 1096 format(2(f8.3,1x),3(i4,1x),f5.2,1x,f6.1,1x,2(i4,1x)) 
                                                                        
  700 continue 
      enddo 
                                                                        
! ======================================================================
! F.Mele: write a file with the selected clusters (magmax greater than a
!         minimum and num-events greater then a minimum)                
      ngood = 0 
      do  k=1,n 
                                                                        
! Versione Kanamori:                                                    
        write(*,*)' k -------- > ',k 
        xmag=(alog10(cmoment(k)))/1.5 
                                                                        
        if (nc(k) .eq. 0) goto 800 
        write(*,*)'  scrivo il cluster n. ',k 
                                                                        
      if( (xmag .ge. rmag_min .and. nc(k) .ge. nc_min )  .or.           &
     &      (cmag1(k) .ge. rmag_suf .and. nc(k) .ge. nc_suf)  )then     
          ngood = ngood +1 
!----  for each cluster loop on events                                  
            do  i=1,neq 
              read (1, rec=i) icent,itime,lat1,ins,xlat1,lon1,iew,xlon1,&
     &                           dep1,xmag1,erh1,erz1,q1,dlat1, dlon1,  &
     &              ddep1, rms, nst, amag1, aflag, fmag1, fflag, dum5,  &
     &                             e1az,e1dip,e1, e2az,e2dip,e2, eid1   
            if(list(i) .eq. k)then 
              write (92,821)                                            &
     &        (itime(jj), jj=1,5),                                      &
     &        float(lat1)+xlat1/60., float(lon1)+xlon1/60.,dep1,xmag1,  &
     &        erh1,erz1,q1, list(i)                                     
            endif 
  801     continue 
          enddo 
      endif 
  800 continue 
      enddo 
  821 format(i4,1x,4(i2.2,1x),3(f8.3,1x),f3.1,t72,2f5.1,t82,a,i5) 
! ======================================================================
      write(*,*)' pause prima del write 22 ' 
      write(22,*)' numero di sequenze con mag equivalente >= ',rmag_min 
      write(22,*)' e NC >= ',nc_min 
      write(22,*)' o con almeno ',nc_suf,' eventi e magmax >=',rmag_suf 
      write(22,*)'       ',ngood 
      close (unit=2,status='keep') 
      close (unit=22,status='keep') 
      close (unit=3,status='keep') 
      close (unit=4,status='keep') 
      close (unit=9,status='keep') 
      close (unit=92,status='keep') 
      close (unit=1,status='delete') 
                                                                        
      write(*,*)' numero di sequenze con mag equivalente >= ',rmag_min 
      write(*,*)' e NC >= ',nc_min 
      write(*,*)' o con almeno ',nc_suf,' eventi e magmax >=',rmag_suf 
      write(*,*)'       ',ngood 
                                                                        
      ! pause 
      stop 
  900 WRITE (2, 902) IREC 
  902 FORMAT (' ***** READ ERROR ON INPUT FILE, LINE ', I8) 
      ! pause 
      stop 
  903 WRITE (2, 904) ibigst(k) 
  904 FORMAT (' ***** READ ERROR ON DIRECT ACCESS FILE, LINE ', I8) 
      ! pause 
      stop 
      END                                           
                                                                        
!********************************************************************** 
!********************************************************************** 
                                                                        
      subroutine ctest (itime,lat1,xlat1,lon1,xlon1,dep1,xmag1,erh1,    &
     &                  erz1,q1,jtime,lat2,xlat2,lon2,xlon2,dep2,xmag2, &
     &                  erh2,erz2,q2,cmag1,cluster,ierradj)             
                                                                        
!      Determine whether event1 and event2 are 'clustered'              
!      according to the radial distance criterion:                      
                                                                        
!            reduced hypocentral distance .le. rtest                    
!                                                                       
!      Hypocentral distance r may be reduced by the hypocentral         
!      uncertainty, or ignored, depending on an option set in the       
!      beginning of the main program.                                   
!      To reduce distance by location error, set ierradj = 2.           
!      Or, to ignore the hypocentral errors, set ierradj = 1.           
                                                                        
      logical cluster 
      character*1 q1,q2,qual(4) 
      integer*2 itime(5),jtime(5) 
      dimension erhqual(4), erhearly(4),erzqual(4), erzearly(4) 
      common/a/ rfact,tau 
                                                                        
      data erhqual/.5,1.,2.5,5./ 
      data erhearly/1.,3.,7.,10./ 
      data erzqual/1.,2.,5.,10./ 
      data erzearly/5.,10.,10.,10./ 
      data qual/'A','B','C','D'/ 
                                                                        
                                                                        
      cluster=.false. 
!-- the interaction distance about an event is defined as               
                                                                        
!         r  =  rfact * a(M, dsigma)                                    
                                                                        
!  where a(M, dsigma) is the radius of a circular crack                 
!  (Kanamori and Anderson, 1975) corresponding to an earthquake of      
!  magnitude M and stress drop dsigma.  The value dsigma = 30 bars      
!  is adopted implicitly in the calculation.                            
                                                                        
!     log a  =  0.4*M - (log(dsigma))/3 - 1.45                          
                                                                        
!  The term (log(dsigma)/3 - 1.45) evaluates to the                     
!  factor 0.011 in calculation below, when dsigma=30 bars.              
!  a is in kilometers.                                                  
!                                                                       
!  quindi:                                                              
!             a = 0.011 * 10^[0.4*M]                                  
!                                                                       
                                                                        
                                                                        
!---- determine hypocentral distance between events                     
      alat=lat1+xlat1/60. 
      alon=lon1+xlon1/60. 
      blat=lat2+xlat2/60. 
      blon=lon2+xlon2/60. 
      call delaz(alat,alon,blat,blon,r,azr) 
       z=abs(dep1-dep2) 
      !! assegno una dif di profon di default perch� ho notato errori 
      !! nelle profondit� ipocentrali che eliminano l'associazione.     
                                                                        
                  !DEFAULT                                              
        z = 0.1 
                                                                        
      r=sqrt(z**2 + r**2) 
                                                                        
      if (z .lt. 0.01 .and. r .lt. 0.01) goto 30 
                                                                        
!---- assign hypocentral errors if a quality code was given             
      if (q1  .ne. ' ') then 
      DO  I=1,4 
    4  IF (Q1 .EQ. QUAL(I)) GOTO 5 
      enddo 
      I=4 
    5 IF (Itime(1) .LT. 70) THEN 
            ERH1 = ERHEARLY(I) 
            ERZ1 = ERZEARLY(I) 
      ELSE 
            ERH1 = ERHQUAL(I) 
            ERZ1 = ERZQUAL(I) 
      ENDIF 
      DO  I=1,4 
   14   IF (Q2 .EQ. QUAL(I)) GOTO 15 
      enddo 
      I=4 
   15 IF (Itime(1) .LT. 70) THEN 
            ERH2 = ERHEARLY(I) 
            ERZ2 = ERZEARLY(I) 
      ELSE 
            ERH2 = ERHQUAL(I) 
            ERZ2 = ERZQUAL(I) 
      ENDIF 
      endif 
                                                                        
!---- reduce hypocentral distance by location uncertainty of both events
!     Note that r can be negative when location uncertainties exceed    
!     hypocentral distance                                              
      if (ierradj .eq. 2) then 
      alpha = atan2(z,r) 
      ca = cos(alpha) 
      sa = sin(alpha) 
      e1 = sqrt(erh1*erh1 * ca*ca + erz1*erz1 * sa*sa) 
      e2 = sqrt(erh2*erh2 * ca*ca + erz2*erz2 * sa*sa) 
      r = r - e1 - e2 
      endif 
                                                                        
!     calculate interaction radius for the first event of the pair      
!     and for the largest event in the cluster associated with          
!     the first event                                                   
                                                                        
!       VERSIONE ORIGINALE: test tra evento e il piu' grande del cluster
!                                                                       
   30 r1 = rfact * 0.011 * 10.**(0.4*xmag1) 
      rmain =      0.011 * 10.**(0.4*cmag1) 
      rtest = r1   + rmain 
                                                                        
!   30 r1 = rfact * 0.011 * 10.**(0.4*xmag1)                            
!      r2 = rfact * 0.011 * 10.**(0.4*xmag2)                            
!      rtest = r1   +  r2                                               
                                                                        
                                                                        
!     limit interaction distance to one crustal thickness               
      if (rtest .gt. 30.) rtest=30. 
                                                                        
!---- test distance criterion                                           
      if (r .le. rtest) cluster=.true. 
      return 
      END                                           
                                                                        
!***********************************************************************
!                                                                       
       subroutine tdif (itime,jtime,dif) 
!      Calculates the time difference (jtime - itime)                   
!      where the elements of itime of jtime represent year,             
!      month, day, hour and minute.  Leap years are accounted for.      
!      The time difference, in minutes, is returned in the              
!      double precision variable dif.                                   
                                                                        
!      dif = (jtime - itime)                                            
                                                                        
      integer*2 days(12),itime(5),jtime(5) 
      data days/0,31,59,90,120,151,181,212,243,273,304,334/ 
      double precision t1,t2,t1a,t2a,t1b,t2b,dif 
                                                                        
!---- t1a is number of minutes from 00:00 1/1/itime(1) to itime         
      t1a = ( (days(itime(2)) + itime(3)-1)*24. + itime(4))*60.         &
     &      + itime(5)                                                  
      if (mod(itime(1),4).eq.0 .and. itime(2).gt.2) t1a = t1a + 1440. 
                                                                        
!---- t1b is number of days from 00:00 1/1/70 to 00:00 1/1/itime(1)     
      t1b = (itime(1)-1970)*365 + int((itime(1)-1970)/4.) 
                                                                        
      t1 = t1a + t1b *1440. 
                                                                        
!---- t2a is number of minutes from 00:00 1/1/jtime(1) to jtime         
      t2a = ( (days(jtime(2)) + jtime(3)-1)*24. + jtime(4))*60.         &
     &        + jtime(5)                                                
      if (mod(jtime(1),4).eq.0 .and. jtime(2).gt.2) t2a = t2a + 1440. 
                                                                        
!---- t2b is number of days from 00:00 1/1/69 to 00:00 1/1/jtime(1)     
      t2b = (jtime(1)-1970)*365 + int((jtime(1)-1970)/4.) 
                                                                        
      t2 = t2a + t2b *1440. 
                                                                        
      dif = t2 - t1 
      return 
      END                                           
                                                                        
                                                                        
!***********************************************************************
!                                                                       
       subroutine tdif2 (itime,jtime,dif) 
!      Calculates the time difference (jtime - itime)                   
!      where the elements of itime of jtime represent year,             
!      month, day, hour and minute.  Leap years are accounted for.      
!      The time difference, in minutes, is returned in the              
!      double precision variable dif.                                   
                                                                        
!      This version (tdif2) uses seconds (assumed truncated in the      
!      data to nearest hundredth of a second)                           
!                                                                       
!      itime(1)=years, itime(2)=mo, ... itime(5)=min, itime(6)=sec*100  
!                                                                       
!            itime(6) will range from 0 to 6000, and can be held        
!            by integer*2 storage.                                      
                                                                        
!      dif = (jtime - itime)                                            
                                                                        
      integer*2 days(12),itime(6),jtime(6) 
      data days/0,31,59,90,120,151,181,212,243,273,304,334/ 
      double precision t1,t2,t1a,t2a,t1b,t2b,dif 
                                                                        
!---- t1a is number of minutes from 00:00 1/1/itime(1) to itime         
      t1a = ( (days(itime(2)) + itime(3)-1)*24. + itime(4))*60.         &
     &      + itime(5)                                                  
      if (mod(itime(1),4).eq.0 .and. itime(2).gt.2) t1a = t1a + 1440. 
                                                                        
!---- t1b is number of days from 00:00 1/1/69 to 00:00 1/1/itime(1)     
      t1b = (itime(1)-1970)*365 + int((itime(1)-1970)/4.) 
                                                                        
      seci = itime(6)/100. 
      t1 =   seci/60. + t1a + t1b *1440. 
                                                                        
!---- t2a is number of minutes from 00:00 1/1/jtime(1) to jtime         
      t2a = ( (days(jtime(2)) + jtime(3)-1)*24. + jtime(4))*60.         &
     &        + jtime(5)                                                
      if (mod(jtime(1),4).eq.0 .and. jtime(2).gt.2) t2a = t2a + 1440. 
                                                                        
!---- t2b is number of days from 00:00 1/1/70 to 00:00 1/1/jtime(1)     
      t2b = (jtime(1)-1970)*365 + int((jtime(1)-1970)/4.) 
                                                                        
      secj = jtime(6)/100. 
      t2 =   secj/60. + t2a + t2b *1440. 
                                                                        
      dif = t2 - t1 
      return 
      END                                           
                                                                        
      subroutine delaz(alat,alon,blat,blon,dist,azr) 
!                                                                       
!        double precision version                                       
!                                                                       
!        computes cartesian distance from a to b                        
!        a and b are in decimal degrees and n-e coordinates             
!        del -- delta in degrees                                        
!        dist -- distance in km                                         
!        az -- azimuth from a to b clockwise from north in degrees      
!                                                                       
      real*8 pi2,rad,flat,alatr,alonr,blatr,blonr,geoa,geob,            &
     &       tana,tanb,acol,bcol,diflon,cosdel,delr,top,den,            &
     &       colat,radius                                               
!                                                                       
      data pi2/1.570796e0/ 
      data rad/1.745329e-02/ 
      data flat/.993231e0/ 
      if (alat.eq.blat.and.alon.eq.blon) goto 10 
!-----convert to radians                                                
      alatr=alat*rad 
      alonr=alon*rad 
      blatr=blat*rad 
      blonr=blon*rad 
      tana=flat*dtan(alatr) 
      geoa=datan(tana) 
      acol=pi2-geoa 
      tanb=flat*dtan(blatr) 
      geob=datan(tanb) 
      bcol=pi2-geob 
!-----calcuate delta                                                    
      diflon=blonr-alonr 
      cosdel=dsin(acol)*dsin(bcol)*dcos(diflon)+dcos(acol)*dcos(bcol) 
      delr=dacos(cosdel) 
!-----calcuate azimuth from a to b                                      
      top=dsin(diflon) 
      den=dsin(acol)/dtan(bcol)-dcos(acol)*dcos(diflon) 
      azr=datan2(top,den) 
!-----compute distance in kilometers                                    
      colat=pi2-(alatr+blatr)/2. 
      radius=6371.227*(1.0+3.37853d-3*(1./3.-((dcos(colat))**2))) 
      dist=delr*radius 
      return 
   10 dist=0.0 
      azr=0.0 
      return 
      END                                           
!                                                                       
!********************************************************************** 
!                                                                       
      subroutine getrec (rec, iunit, istat) 
!--Getrec reads a buffer of ASCII records, the returns them one at a time
                                                                        
      save            nsize 
!                        number of records in buffer (presently 500 max)
      character*(*)      rec 
!                        record to be "read"                            
      integer            iunit 
!                        input unit number                              
      integer            istat 
!                        0 for normal return of a record                
!                        1 end of file reached, no more records         
      save            nrec 
      data nrec /0/ 
!                  current position of last record returned in buffer   
      character*128      buf(500) 
!                        character buffer                               
                                                                        
!--Read in a new buffer if its empty                                    
      if (nrec.eq.0 .or. nrec.eq.nsize) then 
        nrec=0 
        read (iunit, end=9) nsize, (buf(i),i=1,nsize) 
      end if 
                                                                        
      istat=0 
!--Grab the next record from the buffer                                 
      nrec = nrec +1 
      rec = buf(nrec) 
      return 
                                                                        
!--End of file                                                          
    9  istat=1 
      return 
      END                                           
